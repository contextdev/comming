from django.contrib import admin

from .models import Interested

# Register your models here.
admin.site.register(Interested)