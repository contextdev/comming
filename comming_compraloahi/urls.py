from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView

from newsletter.views import RegisterInterested


urlpatterns = patterns('',

    url(r'^$', RegisterInterested.as_view()),
    url(r'^success/$', TemplateView.as_view(template_name='success.html')),
    url(r'^que-es/$', TemplateView.as_view(template_name='que-es.html')),
    url(r'^admin/', include(admin.site.urls)),

)
