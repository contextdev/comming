"""
Django settings for comming_compraloahi project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '3nx3ev656%o$j8*$j4g&**ckcbe3^iuk%3)pd^yo1a6vtm#1dv'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'bootstrap3',
    'newsletter'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    #'django.middleware.cache.CacheMiddleware', # cada página que no tenga parámetros GET o POST será puesta en cache por un cierto período de tiempo la primera vez que sean pedidas.
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

)

ROOT_URLCONF = 'comming_compraloahi.urls'

WSGI_APPLICATION = 'comming_compraloahi.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

# Defined folder of static files to projects
STATICFILES_DIRS = (os.path.join(BASE_DIR, "static"), )

STATIC_ROOT =  os.path.join(os.path.dirname(BASE_DIR), "static")


# Nose
SITE_ROOT = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))


TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, "templates"),
)

# Configuration email
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'testnubiquo@gmail.com'
EMAIL_HOST_PASSWORD = 'nubiquo1234567890'
EMAIL_USE_TLS = True


## CONFIGURATION FOR PRODUCTION

# Configured to send an e-mail to the site developers whenever your code raises an unhandled exception
ADMINS = (
    ('Marcos Barroso', 'mj1182@gmail.com'),
    ('Matias Roson', 'matiroson@gmail.com'),
)

# the option of receiving an e-mail any time somebody visits a page on your Django-powered site that
# raises 404 with a non-empty referrer – that is, every broken link.
MANAGERS = (
    ('Marcos Barroso', 'mj1182@gmail.com'),
    ('Matias Roson', 'matiroson@gmail.com'),
)



# CONFIG CACHE

# El tiempo en segundos que cada página será mantenida en la cache.
CACHE_MIDDLEWARE_SECONDS = 10000

# la cache middleware sólo colocará en cache pedidos anónimos
CACHE_MIDDLEWARE_ANONYMOUS_ONLY = True