from .base import *

DEBUG = False

TEMPLATE_DEBUG = DEBUG

STATIC_ROOT =  os.path.join(os.path.join(os.path.join(os.path.dirname(BASE_DIR), "static")))

ALLOWED_HOSTS = ['*']